public interface IVerifyFileExtension {
    String verifyFileExtension(String extension, String filePath);
}
