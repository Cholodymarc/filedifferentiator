import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class VerifyTxt implements IVerifyFileExtension {

    private FileBytes FileBytes;

    public VerifyTxt() {
        this.FileBytes = new FileBytes();
    }

    //as there is no magic number to verify for txt file,
    //my idea was to execute Git Bash command 'file' from the level of java to obtain below result

    //    $ file TxtCorrect.txt
    //    TxtCorrect.txt: ASCII text, with no line terminators
    //    $ file TxtNotCorrect.txt
    //    TxtNotCorrect.txt: GIF image data, version 89a, 2400 x 2931

    // we can capture the result and compare it to the file extension which we are currently checking
    // this method could be applied to jpg, git and any other extension as well
    @Override
    public String verifyFileExtension(String extension, String filePath) {

        String magicNumber = FileBytes.getFileBytesToString(filePath);
        Charset.forName("US-ASCII").newEncoder().canEncode(magicNumber);

        File txtFile = getFileFromResources("TxtCorrect.txt");

        System.out.println(txtFile);

        try {
            linuxFileCmd(Path.of(txtFile.getPath()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    private File getFileFromResources(String fileName) {

        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }
    }

    public void linuxFileCmd(Path directory) throws IOException, InterruptedException {
        runCommand(directory, "file");
    }

    public void runCommand(Path directory, String... command) throws IOException, InterruptedException {
        Objects.requireNonNull(directory, "directory");
        if (!Files.exists(directory)) {
            throw new RuntimeException("can't run command in non-existing directory '" + directory + "'");
        }
        ProcessBuilder pb = new ProcessBuilder()
                .command(command)
                .directory(directory.toFile());
        Process p = pb.start();
        StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR");
        StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT");
        outputGobbler.start();
        errorGobbler.start();
        int exit = p.waitFor();
        errorGobbler.join();
        outputGobbler.join();
        if (exit != 0) {
            throw new AssertionError(String.format("runCommand returned %d", exit));
        }
    }

    private class StreamGobbler extends Thread {

        private final InputStream is;
        private final String type;

        private StreamGobbler(InputStream is, String type) {
            this.is = is;
            this.type = type;
        }

        @Override
        public void run() {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(is));) {
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println(type + "> " + line);
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

}

