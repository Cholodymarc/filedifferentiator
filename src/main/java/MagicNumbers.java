public enum MagicNumbers {

    JPG1("jpg", 12, "FF D8 FF E0 00 10 4A 46 49 46 00 01"),
    JPG2("jpg", 4, "FF D8 FF DB"),
    JPG3("jpg", 4, "FF D8 FF EE"),
    JPG4("jpg", 4, "FF D8 FF E1"),
    GIF1("gif", 6, "47 49 46 38 37 61"),
    GIF2("gif", 6, "47 49 46 38 39 61");

    private final String fileExtensionName;
    private final int numberOfBytes;
    private final String fileMagicNumber;

    MagicNumbers(String fileExtensionName, int numberOfBytes, String fileMagicNumber) {
        this.fileExtensionName = fileExtensionName;
        this.numberOfBytes = numberOfBytes;
        this.fileMagicNumber = fileMagicNumber;
    }

    public String getFileExtensionName() {
        return fileExtensionName;
    }

    public int getNumberOfBytes() {
        return numberOfBytes;
    }

    public String getFileMagicNumber() {
        return fileMagicNumber;
    }
}