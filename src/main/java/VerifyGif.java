
public class VerifyGif implements IVerifyFileExtension {

    private FileBytes FileBytes;

    public VerifyGif() {
        this.FileBytes = new FileBytes();
    }

    @Override
    public String verifyFileExtension(String extension, String filePath) {
        String magicNumber = FileBytes.getFileBytesToString(filePath);

        if (MagicNumbers.GIF1.getFileMagicNumber().equalsIgnoreCase(magicNumber.substring(0, MagicNumbers.GIF1.getFileMagicNumber().length()))
                || MagicNumbers.GIF2.getFileMagicNumber().equalsIgnoreCase(magicNumber.substring(0, MagicNumbers.GIF2.getFileMagicNumber().length()))) {
            return "Extension is " + extension;
        }
        return "Extension is not " + extension;
    }
}
