import java.util.LinkedHashMap;
import java.util.Map;

public class ExpectedFileExtensions {
    private Map<String, IVerifyFileExtension> fileExtensions;

    public ExpectedFileExtensions() {
        this.fileExtensions = new LinkedHashMap<>();
    }

    public void addExtension(String extensionName, IVerifyFileExtension fileExtension) {
        this.fileExtensions.put(extensionName, fileExtension);
    }

    public IVerifyFileExtension getFileExtension(String name) {
        if (fileExtensions.containsKey(name)) {
            return fileExtensions.get(name);
        } else {
            throw new IllegalArgumentException("This is not exception handled in this program.");
        }
    }
}
