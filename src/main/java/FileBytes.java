import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileBytes {
    String getFileBytesToString(String filePath) {

        Path path = Paths.get(filePath);
        try {
            byte[] fileBytes = Files.readAllBytes(path);

            StringBuilder stringBuilder = new StringBuilder();
            for (byte fileByte : fileBytes) {
                stringBuilder.append(String.format("%02X ", fileByte));
            }
            System.out.println(stringBuilder.toString());
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}