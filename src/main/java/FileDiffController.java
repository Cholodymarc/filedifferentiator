import java.io.File;
import java.net.URL;

public class FileDiffController implements IFileDiffController {

    private ExpectedFileExtensions expectedFileExtensions;

    public FileDiffController() {
        this.expectedFileExtensions = new ExpectedFileExtensions();
    }

    @Override
    public void start() {
        loadHandledExtensions();
        String correctGifPath = "GifCorrect.gif";
        String notCorrectGifPath = "GifNotCorrect.gif";

        String correctJpgPath = "JpgCorrect.jpg";
        String notCorrectJpgPath = "JpgNotCorrect.jpg";

        String correctTxtPath = "TxtCorrect.txt";
        String notCorrectTxtPath = "TxtNotCorrect.txt";

        System.out.println(checkFileTypeByExtension(correctGifPath));
        System.out.println(checkFileTypeByExtension(notCorrectGifPath));
        System.out.println(checkFileTypeByExtension(correctJpgPath));
        System.out.println(checkFileTypeByExtension(notCorrectJpgPath));
        System.out.println(checkFileTypeByExtension(correctTxtPath));
        System.out.println(checkFileTypeByExtension(notCorrectTxtPath));

    }

    private void loadHandledExtensions() {
        expectedFileExtensions.addExtension("txt", new VerifyTxt());
        expectedFileExtensions.addExtension("jpg", new VerifyJpg());
        expectedFileExtensions.addExtension("gif", new VerifyGif());
    }

    private String checkFileTypeByExtension(String filePath) {
        return expectedFileExtensions.getFileExtension(getExtension(filePath)).verifyFileExtension(getExtension(filePath), filePath);
    }

    private String getExtension(String filePath) {
        String fileExtension = "";
        int i = filePath.lastIndexOf('.');
        if (i > 0) {

            fileExtension = filePath.substring(i + 1);
        }
        return fileExtension;
    }

    private File getFileFromResources(String fileName) {

        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }
    }
}
