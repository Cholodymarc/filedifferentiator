public class VerifyJpg implements IVerifyFileExtension {

    private FileBytes FileBytes;

    public VerifyJpg() {
        this.FileBytes = new FileBytes();
    }

    @Override
    public String verifyFileExtension(String extension, String filePath) {
        String magicNumber = FileBytes.getFileBytesToString(filePath);

        if (magicNumber.length() < MagicNumbers.JPG2.getFileMagicNumber().length()) {
            return "Extension is not " + extension;
        }

        if (MagicNumbers.JPG1.getFileMagicNumber().equalsIgnoreCase(magicNumber.substring(0, MagicNumbers.JPG1.getFileMagicNumber().length()))
                || MagicNumbers.JPG2.getFileMagicNumber().equalsIgnoreCase(magicNumber.substring(0, MagicNumbers.JPG2.getFileMagicNumber().length()))
                || MagicNumbers.JPG3.getFileMagicNumber().equalsIgnoreCase(magicNumber.substring(0, MagicNumbers.JPG3.getFileMagicNumber().length()))
                || MagicNumbers.JPG4.getFileMagicNumber().equalsIgnoreCase(magicNumber.substring(0, MagicNumbers.JPG4.getFileMagicNumber().length()))) {
            return "Extension is " + extension;
        }
        return "Extension is not " + extension;
    }
}
